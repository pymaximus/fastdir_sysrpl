Saturn Assembler                                       Sun Sep  8 22:31:22 2019
V3.0.8 (12/06/2002)                                      fd.a           Page    1

    1             * -*- mode: sysrpl -*-
    2             *
    3             * SysRPL version of fastdir.rpl
    4             * Copyright (C) 2019 Frank Singleton b17flyboy@gmail.com
    5             *
    6             * Include the header file KEYDEFS.H, which defines words
    7             * like kcUpArrow at physical key numbers.
    8             *
    9             * File  fd.s    9
   10             *|| Reading from KEYDEFS.H
   11             * ASSEMBLE
   12                     CLRLIST ALL
   13             * RPL
   14             * ASSEMBLE
   16             * RPL
   17             * RPL
   18             * insure RPL-mode exit
   19             *|| Resuming file fd.s at line 9
   20             * ASSEMBLE
   21             * File  fd.s    14
   22 00000 84058         NIBASC  /HPHP49-C/
            40543
            93D234
   23             * File  fd.s    15
   24             * RPL
   25             *
   26             * Begin the secondary
   27             *
   28             * File  fd.s    19
   29 00010 00000         CON(5)  =DOCOL
   30             * File  fd.s    20
   31 00015 00000         CON(5)  =CK0NOLASTWD
   32             * no args required
   33             * File  fd.s    21
   34 0001A 00000         CON(5)  =DOVARS
   35             * File  fd.s    22
   36 0001F 00000         CON(5)  =DUP
   37             * File  fd.s    23
   38 00024 00000         CON(5)  =xSIZE
   39             * File  fd.s    24
   40 00029 00000         CON(5)  =DOLIST
   41             * File  fd.s    25
   42 0002E 00000         CON(5)  =DOLAM
   43 00033 40            CON(2)  4
   44 00035 67162         NIBASC  \vars\
            737
   45             * File  fd.s    26
   46 0003D 00000         CON(5)  =DOLAM
   47 00042 50            CON(2)  5
   48 00044 67379         NIBASC  \vsize\
            6A756
   49             * File  fd.s    27
   50 0004E 00000         CON(5)  =SEMI
   51             * File  fd.s    28
   52 00053 00000         CON(5)  =BIND
   53             * File  fd.s    29
   54 00058 00000         CON(5)  =DOLAM
   55 0005D 50            CON(2)  5
   56 0005F 67379         NIBASC  \vsize\
            6A756
   57             * 'vsize' RCL
   58             * File  fd.s    30
   59 00069 00000         CON(5)  =%0<>
   60             * convert to TRUE/FALSE
   61             * File  fd.s    31
   62 0006E 00000         CON(5)  =IT
   63             * File  fd.s    32
   64 00073 00000         CON(5)  =DOCOL
   65             * File  fd.s    33
   66 00078 00000         CON(5)  =DOLAM
   67 0007D 50            CON(2)  5
   68 0007F 67379         NIBASC  \vsize\
            6A756
   69             * File  fd.s    34
   70 00089 00000         CON(5)  =COERCE
   71             * convert to BINT for DO loop
   72             * File  fd.s    35
   73 0008E 00000         CON(5)  =#1+
   74             * to include last element
   75             * File  fd.s    36
   76 00093 00000         CON(5)  =BINT1
   77             * File  fd.s    37
   78 00098 00000         CON(5)  =DO
   79             * File  fd.s    38
   80 0009D 00000         CON(5)  =DOCOL
   81             * File  fd.s    39
   82 000A2 00000         CON(5)  =DOLAM
   83 000A7 40            CON(2)  4
   84 000A9 67162         NIBASC  \vars\
            737
   85             * File  fd.s    40
   86 000B1 00000         CON(5)  =INDEX@
   87 000B6 00000         CON(5)  =UNCOERCE
   88             * convert BINT INDEX to real for xGET
   89             * File  fd.s    41
   90 000BB 00000         CON(5)  =xGET
   91 000C0 00000         CON(5)  =DUP
   92 000C5 00000         CON(5)  =xRCL
   93 000CA 00000         CON(5)  =xTYPE
   94 000CF 00000         CON(5)  =COERCE
   95             * File  fd.s    42
   96 000D4 00000         CON(5)  =DOLIST
   97 000D9 00000         CON(5)  =DOLAM
   98 000DE 50            CON(2)  5
   99 000E0 56E61         NIBASC  \ename\
            6D656
  100 000EA 00000         CON(5)  =DOLAM
  101 000EF 50            CON(2)  5
  102 000F1 56479         NIBASC  \etype\
            70756
  103 000FB 00000         CON(5)  =SEMI
  104 00100 00000         CON(5)  =BIND
  105             * File  fd.s    43
  106 00105 00000         CON(5)  =DOLAM
  107 0010A 50            CON(2)  5
  108 0010C 56E61         NIBASC  \ename\
            6D656
  109 00116 00000         CON(5)  =DOLAM
  110 0011B 50            CON(2)  5
  111 0011D 56479         NIBASC  \etype\
            70756
  112 00127 00000         CON(5)  =#>$
  113 0012C 00000         CON(5)  =>TAG
  114             * File  fd.s    44
  115 00131 00000         CON(5)  =SEMI
  116             * File  fd.s    45
  117 00136 00000         CON(5)  =LOOP
  118             * File  fd.s    46
  119 0013B 00000         CON(5)  =SEMI
  120             * File  fd.s    47
  121 00140 00000         CON(5)  =DOLAM
  122 00145 50            CON(2)  5
  123 00147 67379         NIBASC  \vsize\
            6A756
  124 00151 00000         CON(5)  =COERCE
  125             * convert size to BINT
  126             * File  fd.s    48
  127 00156 00000         CON(5)  ={}N
  128             * and make list from stack
  129             * File  fd.s    49
  130 0015B 00000         CON(5)  =SEMI
Saturn Assembler                                       Sun Sep  8 22:31:22 2019
V3.0.8 (12/06/2002)   Symbol Table                       fd.a           Page    2

 #1+                               Ext                   -    73
 #>$                               Ext                   -   112
 %0<>                              Ext                   -    59
 >TAG                              Ext                   -   113
 BIND                              Ext                   -    52   104
 BINT1                             Ext                   -    76
 CK0NOLASTWD                       Ext                   -    31
 COERCE                            Ext                   -    70    94   124
 DO                                Ext                   -    78
 DOCOL                             Ext                   -    29    64    80
 DOLAM                             Ext                   -    42    46    54
                                          66    82    97   100   106   109   121
 DOLIST                            Ext                   -    40    96
 DOVARS                            Ext                   -    34
 DUP                               Ext                   -    36    91
 INDEX@                            Ext                   -    86
 IT                                Ext                   -    62
 LOOP                              Ext                   -   117
 SEMI                              Ext                   -    50   103   115
                                         119   130
 UNCOERCE                          Ext                   -    87
 xGET                              Ext                   -    90
 xRCL                              Ext                   -    92
 xSIZE                             Ext                   -    38
 xTYPE                             Ext                   -    93
 {}N                               Ext                   -   127
Saturn Assembler                                       Sun Sep  8 22:31:22 2019
V3.0.8 (12/06/2002)   Statistics                         fd.a           Page    3

Input Parameters

  Source file name is fd.a

  Listing file name is fd.l

  Object file name is fd.o

  Flags set on command line
    None

Warnings:

  None

Errors:

  None
