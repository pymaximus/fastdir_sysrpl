* -*- mode: sysrpl -*-
*
* SysRPL version of fastdir.rpl
* Copyright (C) 2019 Frank Singleton b17flyboy@gmail.com
*
* Include the header file KEYDEFS.H, which defines words
* like kcUpArrow at physical key numbers.
*
INCLUDE KEYDEFS.H
*
* Include the eight characters needed for binary download
*
ASSEMBLE
        NIBASC	/HPHP49-C/
RPL
*
* Begin the secondary
*
::
        CK0NOLASTWD           ( no args required )
        DOVARS
        DUP
        xSIZE
        {
        LAM vars
        LAM vsize
        }
        BIND
        LAM vsize    ( 'vsize' RCL )
        %0<>            ( convert to TRUE/FALSE)
        IT
        ::
          LAM vsize
          COERCE  ( convert to BINT for DO loop)
          #1+     ( to include last element)
          BINT1
          DO
          ::
            LAM vars
            INDEX@ UNCOERCE     ( convert BINT INDEX to real for xGET)
            xGET DUP xRCL xTYPE COERCE
            { LAM ename LAM etype } BIND
            LAM ename LAM etype #>$ >TAG
          ;
          LOOP
        ;
        LAM vsize COERCE        ( convert size to BINT )
        {}N                     ( and make list from stack )
;
