fastdir
=======

This is a port of FASTDIR (User RPL) to Sys RPL. It is not optimal, but 
serves as an example of how local variables in User RPL can be expressed as
LAM's (lambdas) in System RPL.

User RPL
--------

``` text
'FASTDIR' PURGE
\<<
    VARS DUP SIZE       @ fetch vars list and size
    \-> vars vsize      @ save in local variables
    \<<
      IF vsize 0 > THEN
        1 vsize
        FOR I
          @ get var and object type
          vars I GET DUP RCL TYPE \-> ename etype
            \<< etype R\->I ename \->TAG \>>
        NEXT
      END
    \>>
\>>
'FASTDIR' STO

```

System RPL
----------

``` text
* -*- mode: sysrpl -*-
*
* SysRPL version of fastdir.rpl
* Copyright (C) 2019 Frank Singleton b17flyboy@gmail.com
*
* Include the header file KEYDEFS.H, which defines words
* like kcUpArrow at physical key numbers.
*
INCLUDE KEYDEFS.H
*
* Include the eight characters needed for binary download
*
ASSEMBLE
        NIBASC	/HPHP49-C/
RPL
*
* Begin the secondary
*
::
        CK0NOLASTWD           ( no args required )
        DOVARS
        DUP
        xSIZE
        {
        LAM vars
        LAM vsize
        }
        BIND
        LAM vsize    ( 'vsize' RCL )
        %0<>            ( convert to TRUE/FALSE)
        IT
        ::
          LAM vsize
          COERCE  ( convert to BINT for DO loop)
          #1+     ( to include last element)
          BINT1
          DO
          ::
            LAM vars
            INDEX@ UNCOERCE     ( convert BINT INDEX to real for xGET)
            xGET DUP xRCL xTYPE COERCE
            { LAM ename LAM etype } BIND
            LAM ename LAM etype #>$ >TAG
          ;
          LOOP
        ;
        LAM vsize COERCE        ( convert size to BINT )
        {}N                     ( and make list from stack )
;

```


Pretty Print
------------

Until I get a Pygments lexer running, this will suffice..

a2ps --columns=1 -R -C -o fd.ps fd.s

